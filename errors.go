package oauth

import "gitlab.com/thegalabs/go/errors"

// The possible error codes
const (
	InternalError        errors.Code = "internal_error"
	InvalidRequest       errors.Code = "invalid_request"
	InvalidClient        errors.Code = "invalid_client"
	InvalidGrant         errors.Code = "invalid_grant"
	UnauthorizedClient   errors.Code = "unauthorized_client"
	UnsupportedGrantType errors.Code = "unsupported_grant_type"
	InvalidScope         errors.Code = "invalid_scope"
)
