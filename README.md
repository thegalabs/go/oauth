# OAuth

A simple opinionated packaged oauth library.

## Features

### Signing

There are multiple levels of abstractions to handle signatures.

First, the [signer](./signer.go) interface is used to provide a unique way to sign tokens. This is what you would
override if you wanted to use a KMS.

The [default signer implementation](./keys/signer.go) handles signing itself and manages a key ring. In most cases,
the key ring will need to be shared accross threads and instances of the same application.

The [Provider](./keys/signer.go#15) interface's role is to handling building and / or retrieving the key. There are
multiple provided implementations:
- [redis](./keys/providers/redis/provider.go) stores the key ring in a redis memory store. This is probably the fastest
and most efficient implementation but it also involves provisioning a redis to store one blob of data...
- [database](./keys/providers/database/provider.go) stores the key ring in a database. It is slightly slower than redis
but since most apps will also be connected to a database it does not incur additional setup or costs
- [bucket](./keys/providers/bucket/provider.go) uses a file in a gcp bucket. This does not require provisioning any
infrastructure but it also pretty slow (~500ms per Provide operation in GCP)/
- [local](./keys/providers/local/provider.go) just builds a new key ring every time, you should probably only use
that for development / testing.

## Misc

### Testing Cloud storage credentials

See https://cloud.google.com/storage/docs/reference/libraries#command-line

The role that is needed is: `roles/storage.objectCreator`