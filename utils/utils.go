// Package utils contains utils functions that are used throughout the library
package utils

import (
	"crypto/rand"
	"encoding/base64"
	"math"
	"net/http"
	"regexp"
	"strings"

	"gitlab.com/thegalabs/go/errors"
)

// B64Encode is an equivalent to EncodeToString that returns bytes
// and is appropriate for JWTs
func B64Encode(raw []byte) []byte {
	encoded := make([]byte, base64.RawURLEncoding.EncodedLen(len(raw)))
	base64.RawURLEncoding.Encode(encoded, raw)
	return encoded
}

// B64Decode is an equivalent to DecodeToString that returns bytes
// and is appropriate for JWTs
func B64Decode(raw []byte) []byte {
	decoded := make([]byte, base64.RawURLEncoding.DecodedLen(len(raw)))
	_, _ = base64.RawURLEncoding.Decode(decoded, raw)
	return decoded
}

var notSlugRegex = regexp.MustCompile(`[^a-z0-9-]`)

// IsValidSlug returns true if the string is a valid slug
func IsValidSlug(s string) bool {
	match := notSlugRegex.Find([]byte(s))
	return match == nil
}

// ParseBasicAuth extracts the username and password from a basic auth header
func ParseBasicAuth(auth string) (username string, password string, err error) {
	if !strings.HasPrefix(auth, "Basic ") {
		err = errors.NoResponse(http.StatusUnauthorized)
		return
	}

	b, err := base64.StdEncoding.DecodeString(auth[6:])
	if err != nil {
		return
	}

	parts := strings.SplitN(string(b), ":", 3)
	if len(parts) != 2 {
		err = errors.NoResponse(http.StatusUnauthorized)
		return
	}

	username = parts[0]
	password = parts[1]
	return
}

// RandomString returns a crypto safe random string
func RandomString(length int) (string, error) {
	buff := make([]byte, int(math.Round(float64(length)/float64(1.33333333333)))) //nolint: gomnd
	if _, err := rand.Read(buff); err != nil {
		return "", err
	}
	str := base64.RawURLEncoding.EncodeToString(buff)
	return str[:length], nil
}
