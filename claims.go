package oauth

import (
	"encoding/json"
	"time"

	"github.com/gofrs/uuid"

	"gitlab.com/thegalabs/go/oauth/utils"
)

// Claims reprensent the main part of the JWT
type Claims struct {
	TokenID   uuid.UUID `json:"tid"`
	Issuer    string    `json:"iss"`
	Audience  string    `json:"aud"`
	Entity    string    `json:"ent,omitempty"`
	Subject   string    `json:"sub,omitempty"`
	Scope     []string  `json:"scope,omitempty"`
	IssuedAt  int64     `json:"iat"`
	ExpiresAt int64     `json:"exp"`
}

// SetIssuerIfNeeded sets the issuer if it is empty
func (c *Claims) SetIssuerIfNeeded(issuer string) {
	if len(c.Issuer) == 0 {
		c.Issuer = issuer
	}
}

// ApplyLifetime sets the issuedAt and ExpiresAt accordingly
func (c *Claims) ApplyLifetime(lifetime int64) {
	iat := time.Now().Unix()
	c.IssuedAt = iat
	c.ExpiresAt = iat + lifetime
}

// LimitScope removes all scope that are not in array
// This method is used when authenticating, to make sure that a token does not have more privileges
// than the client that generated it.
func (c *Claims) LimitScope(scope []string) {
	var filtered []string
	for _, s := range c.Scope {
		for _, t := range scope {
			if s == t {
				filtered = append(filtered, s)
				break
			}
		}
	}
	c.Scope = filtered
}

// Encode outputs data that can be used in a jwt
func (c *Claims) Encode() ([]byte, error) {
	raw, err := json.Marshal(c)
	if err != nil {
		return nil, err
	}
	return utils.B64Encode(raw), nil
}
