// Package database stores keys in a gorm.db
package database

import (
	"crypto/cipher"
	"errors"

	"gorm.io/gorm"

	"gitlab.com/thegalabs/go/oauth/keys"
	"gitlab.com/thegalabs/go/oauth/keys/providers/utils"
)

// Provider uses a database to store the keys
type Provider struct {
	db  *gorm.DB
	gcm cipher.AEAD
}

type keyRing struct {
	ID       uint `gorm:"primarykey"`
	Data     []byte
	Revision uint `gorm:"index"`
}

const keyRingID = 1

func (p *Provider) read() ([]byte, uint, error) {
	var r keyRing
	if err := p.db.First(&r, keyRingID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, 0, nil
		}
		return nil, 0, err
	}
	return r.Data, r.Revision, nil
}

func (p *Provider) write(data []byte, revision uint) error {
	if revision == 0 { // Ring did not exist in database, creating it
		return p.db.Create(&keyRing{ID: keyRingID, Data: data, Revision: 1}).Error
	}
	// Updating only if data was not updated in the mean time
	res := p.db.
		Model(&keyRing{ID: keyRingID}).
		Where("revision = ?", revision).
		Updates(map[string]interface{}{"revision": revision + 1, "data": data})

	if res.Error != nil {
		return res.Error
	}
	if res.RowsAffected == 0 {
		return ErrKeyWasChanged
	}
	return nil
}

func (p *Provider) transaction(
	r *keys.KeyRing,
	now func() keys.Now,
	benchTime keys.BenchTime,
	lifeTime keys.LifeTime,
	keepFor keys.KeepFor) (newRing *keys.KeyRing, err error) {
	t := now()

	var b []byte
	var gen uint
	if b, gen, err = p.read(); err != nil {
		return
	}

	// There was no key
	if b == nil {
		if r != nil {
			newRing = r
		} else {
			newRing = keys.NewRing()
		}
	} else {
		if newRing, err = utils.RingFromSecret(b, p.gcm); err != nil {
			return
		}
		if !newRing.IsDirty(t, benchTime) {
			// It works -> great, we can use it, no need to rewrite it
			return
		}
	}

	// Cleanup whatever ring we have
	if err = newRing.Cleanup(t, benchTime, lifeTime, keepFor); err != nil {
		return
	}

	if b, err = utils.RingToSecret(newRing, p.gcm); err != nil {
		return
	}

	err = p.write(b, gen)

	return
}

// New makes a bucket provider
func New(db *gorm.DB, key string) (keys.KeyRingProvider, error) {
	gcm, err := utils.MakeCipher(key)
	if err != nil {
		return nil, err
	}

	if err = db.AutoMigrate(&keyRing{}); err != nil {
		return nil, err
	}

	return &Provider{db, gcm}, nil
}

// Provide returns a key stored in the database
func (p *Provider) Provide(
	r *keys.KeyRing,
	now func() keys.Now,
	benchTime keys.BenchTime,
	lifeTime keys.LifeTime,
	keepFor keys.KeepFor) (ring *keys.KeyRing, err error) {
	for i := 0; i < 5; i++ {
		ring, err = p.transaction(r, now, benchTime, lifeTime, keepFor)
		if err == ErrKeyWasChanged {
			continue
		}
		return
	}

	return
}

// ErrKeyWasChanged is returned when the generation does not match
var ErrKeyWasChanged = errors.New("key was changed since read")
