package bucket

import (
	"context"
	"errors"
	"io/ioutil"
	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/api/googleapi"
)

// object is a simple wrapper to make testing easier
type object interface {
	Read() ([]byte, int64, error)
	Write([]byte, int64) error
}

type gcpObject struct {
	ctx    context.Context
	client *storage.Client
	obj    *storage.ObjectHandle

	timeout time.Duration
}

func newObject(ctx context.Context, bucketName string, objName string, timeout time.Duration) (object, error) {
	client, err := storage.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	bkt := client.Bucket(bucketName)

	return &gcpObject{
		ctx,
		client,
		bkt.Object(objName),
		timeout,
	}, nil
}

func (o *gcpObject) Read() (b []byte, gen int64, err error) {
	ctx, cancel := context.WithTimeout(o.ctx, o.timeout)
	defer cancel()

	r, err := o.obj.NewReader(ctx)
	if err != nil {
		if err == storage.ErrObjectNotExist {
			err = nil
		}
		return
	}
	defer r.Close()

	b, err = ioutil.ReadAll(r)
	gen = r.Attrs.Generation
	return
}

func (o *gcpObject) Write(b []byte, gen int64) error {
	ctx, cancel := context.WithTimeout(o.ctx, o.timeout)
	defer cancel()

	obj := o.obj
	if gen != 0 {
		obj = obj.If(storage.Conditions{GenerationMatch: gen})
	}
	w := obj.NewWriter(ctx)

	if _, err := w.Write(b); err != nil {
		return err
	}

	if err := w.Close(); err != nil {
		if e, ok := err.(*googleapi.Error); ok && e.Code == 412 {
			return ErrKeyWasChanged
		}
		return err
	}
	return nil
}

// ErrKeyWasChanged is returned when the generation does not match
var ErrKeyWasChanged = errors.New("key was changed since read")
