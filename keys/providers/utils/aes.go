// Package utils has functions used to store and retrieve keys
package utils

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"io"
)

// MakeCipher creates a cipher from a base64 encoded string
func MakeCipher(key string) (_ cipher.AEAD, err error) {
	b, err := base64.StdEncoding.DecodeString(key)
	if err != nil {
		return
	}

	block, err := aes.NewCipher(b)
	if err != nil {
		return
	}

	return cipher.NewGCM(block)
}

func encrypt(raw []byte, gcm cipher.AEAD) ([]byte, error) {
	nonce := make([]byte, gcm.NonceSize())
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	// Data is appended to nonce
	return gcm.Seal(nonce, nonce, raw, nil), nil
}

func decrypt(raw []byte, gcm cipher.AEAD) ([]byte, error) {
	nonceSize := gcm.NonceSize()
	nonce, enc := raw[:nonceSize], raw[nonceSize:]

	return gcm.Open(nil, nonce, enc, nil)
}
