// Package jwks allows converting keys to JSON Web Key Sets
package jwks

import (
	"gitlab.com/thegalabs/go/oauth/keys"
)

// ECJWKS represents a set of PublicECJWK
type ECJWKS struct {
	Keys []ECJWK `json:"keys"`
}

// From converts an array of public keys to a json web key set
func From(keys []keys.PublicKey) (*ECJWKS, error) {
	set := new(ECJWKS)
	for _, k := range keys {
		jwk, err := FromKey(k)
		if err != nil {
			return nil, err
		}
		set.Keys = append(set.Keys, *jwk)
	}
	return set, nil
}
