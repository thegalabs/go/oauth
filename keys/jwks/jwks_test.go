package jwks_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/thegalabs/go/oauth/keys"
	"gitlab.com/thegalabs/go/oauth/keys/jwks"
)

func TestFrom(t *testing.T) {
	set, err := jwks.From(
		[]keys.PublicKey{
			newPublicKey(t),
			newPublicKey(t),
		},
	)

	assert.Nil(t, err)
	assert.Equal(t, 2, len(set.Keys))
}
