package keys

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/gob"
	mrand "math/rand"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	now := Now(time.Now().Unix())
	ring := &KeyRing{}

	assert.True(t, ring.IsDirty(now, 100))

	err := ring.Cleanup(now, 100, 10, 1)
	assert.Nil(t, err)

	assert.False(t, ring.IsDirty(now, 100))
}

func compareRings(t *testing.T, expected *KeyRing, actual *KeyRing) {
	compareKey(t, expected.active, actual.active)
	compareKey(t, expected.benched, actual.benched)

	for i := range expected.expired {
		comparePublicKey(t, &expected.expired[i], &actual.expired[i])
	}
}

func newPublicKey(t *testing.T) PublicKey {
	private, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	assert.Nil(t, err)
	return PublicKey{
		uuid.Must(uuid.NewV4()).String(),
		mrand.Int63(), //nolint: gosec
		&private.PublicKey,
	}
}

func TestGob(t *testing.T) {
	generator := func(maker func(*KeyRing)) func(*testing.T) {
		return func(t *testing.T) {
			ring := KeyRing{}
			maker(&ring)

			var network bytes.Buffer

			enc := gob.NewEncoder(&network)
			err := enc.Encode(ring)
			assert.Nil(t, err)

			dec := gob.NewDecoder(&network)
			var v KeyRing
			err = dec.Decode(&v)
			assert.Nil(t, err)

			compareRings(t, &ring, &v)
		}
	}

	t.Run("Full set", generator(func(r *KeyRing) {
		r.active, _ = r.newKey(100, 200)
		r.benched, _ = r.newKey(200, 100)
		r.expired = []PublicKey{
			newPublicKey(t),
			newPublicKey(t),
		}
	}))

	t.Run("No active", generator(func(r *KeyRing) {
		r.benched, _ = r.newKey(200, 100)
		r.expired = []PublicKey{
			newPublicKey(t),
			newPublicKey(t),
		}
	}))

	t.Run("No Benched", generator(func(r *KeyRing) {
		r.active, _ = r.newKey(200, 100)
		r.expired = []PublicKey{
			newPublicKey(t),
			newPublicKey(t),
		}
	}))

	t.Run("No Empty", generator(func(r *KeyRing) {}))

	t.Run("No Expired", generator(func(r *KeyRing) {
		r.active, _ = r.newKey(200, 100)
		r.expired = []PublicKey{}
	}))
}

func TestRotation(t *testing.T) {
	const bench = BenchTime(250)
	const life = LifeTime(500)
	const keepFor = KeepFor(100)

	ring := &KeyRing{}

	cleanup := func(now Now) {
		err := ring.Cleanup(now, bench, life, keepFor)
		assert.Nil(t, err)
	}

	cleanup(0)

	k1 := *ring.active
	assert.NotNil(t, k1)

	// Test benching

	assert.True(t, ring.shouldBench(251, bench))

	cleanup(251)

	k1.notAfter = 501
	compareKey(t, &k1, ring.active)
	assert.NotNil(t, ring.benched)

	// Test rotation

	b1 := *ring.benched

	assert.True(t, ring.shouldRotate(502))

	cleanup(502)
	assert.Nil(t, ring.benched)
	b1.notAfter = 1002
	compareKey(t, &b1, ring.active)

	assert.Equal(t, 1, len(ring.expired))
	assert.Equal(t, k1.id, ring.expired[0].ID)
	assert.Equal(t, int64(602), ring.expired[0].ExpireAt)

	cleanup(603)
	compareKey(t, &b1, ring.active)
	assert.Equal(t, 0, len(ring.expired))
}
