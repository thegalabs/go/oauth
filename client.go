package oauth

import (
	"fmt"
	"os"

	"github.com/gofrs/uuid"

	"gitlab.com/thegalabs/go/oauth/utils"
)

// Client is a base interface for an OAuth client
type Client interface {
	GetID() string
	GetSlug() string
	GetEntity() string

	GetScope(GrantType) []string
	Verify(string) bool

	Validate() error

	BaseClaims() *Claims
}

// BaseClient provides the base implementation for a Client
type BaseClient struct {
	// client_id for OAuth2 purposes
	ID string `json:"id"`
	// client_secret for OAuth2 purposes
	Secret string `json:"secret"`

	// A slug to uniquely identify the client. For example: `iota-android`
	Slug string `json:"slug"`

	// An entity to group clients together. For example: `iota`
	Entity string `json:"entity"`

	// A map grant_type / roles
	Scopes map[GrantType][]string `json:"scopes"`
}

// GetID returns the client_id for OAuth2 purposes
func (c *BaseClient) GetID() string {
	return c.ID
}

// GetSlug is a friendly name for the client that will be used as the aud claim
func (c *BaseClient) GetSlug() string {
	return c.Slug
}

// GetEntity is a friendly name for a group of clients
func (c *BaseClient) GetEntity() string {
	return c.Entity
}

// GetScope returns the client scope for a given grant type
func (c *BaseClient) GetScope(grantType GrantType) []string {
	return c.Scopes[grantType]
}

// Verify tries and match the client secret with the provided
func (c *BaseClient) Verify(secret string) bool {
	return c.Secret == secret
}

// Validate checks that a client is valid
func (c *BaseClient) Validate() error {
	if len(c.Slug) == 0 || !utils.IsValidSlug(c.Slug) {
		return fmt.Errorf("invalid Client Slug %s", c.Slug)
	}

	if len(c.Entity) == 0 || !utils.IsValidSlug(c.Entity) {
		return fmt.Errorf("invalid Client Entity `%s`", c.Entity)
	}

	if len(c.ID) != 32 {
		return fmt.Errorf("invalid Client ID %s: should 32 chars", c.ID)
	}

	if len(c.Secret) != 64 {
		return fmt.Errorf("invalid Client Slug %s: should 64 chars", c.ID)
	}

	return nil
}

// BaseClaims instantiates a claim object with client properties prefilled
// It tries to find the issuer in a JWT_ISS env variable and applies a default lifetime of 1h
func (c *BaseClient) BaseClaims() *Claims {
	claims := new(Claims)
	claims.TokenID = uuid.Must(uuid.NewV4())
	claims.Audience = c.Slug
	claims.Entity = c.Entity
	claims.Issuer, _ = os.LookupEnv("JWT_ISS")
	claims.ApplyLifetime(60 * 60)
	return claims
}
