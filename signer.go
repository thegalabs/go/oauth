package oauth

import (
	"gitlab.com/thegalabs/go/oauth/keys"
)

// Signer is the interface for a key manager that rotates keys and signs tokens
type Signer interface {
	// Returns how long a token signed with this can live
	TokenLifetime() int64

	// Returns the active key ID
	// This method should ensure that the key is valid
	KeyID() (string, error)

	// Sign returns a cryptographic signature
	// The KeyID should be retrieved right before this call using KeyID()
	Sign(string, []byte) ([]byte, error)

	// PublicKeys return the authorized public keys, it also rotates the keys if required
	// Also returns the remaining time for which the keys will be valid
	PublicKeys() ([]keys.PublicKey, int64, error)
}
