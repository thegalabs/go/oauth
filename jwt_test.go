package oauth_test

import (
	"strings"
	"testing"

	"gitlab.com/thegalabs/go/oauth"
	"gitlab.com/thegalabs/go/oauth/keys"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
)

type dummyProvider struct{}

func (*dummyProvider) Provide(
	_ *keys.KeyRing,
	now func() keys.Now,
	b keys.BenchTime,
	l keys.LifeTime,
	k keys.KeepFor) (*keys.KeyRing, error) {
	r := keys.NewRing()
	if err := r.Cleanup(now(), b, l, k); err != nil {
		return nil, err
	}
	return r, nil
}

func TestMake(t *testing.T) {
	claims := oauth.Claims{uuid.Must(uuid.NewV4()), "thegalabs.fr", "clt1-a", "clt1", "", []string{"user", "client"}, 0, 100}
	manager, _ := keys.NewSigner(10, 100, 1000, &dummyProvider{})

	token, err := oauth.MakeJWT(&claims, manager)

	assert.Nil(t, err)

	parts := strings.Split(token, ".")
	assert.Equal(t, 3, len(parts))
}
