// Package oauth provides a barebone implementation of an OAuth2 with JWT
package oauth

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"gitlab.com/thegalabs/go/oauth/utils"
)

// Auth is the base struct
type Auth struct {
	Issuer  string
	Clients map[string]Client

	Signer Signer
}

// NewAuth is the default Auth Constructor, the client list is validated
func NewAuth(issuer string, clients []Client, manager Signer) (auth *Auth, err error) {
	auth = new(Auth)
	auth.Issuer = issuer
	if err := auth.SetClients(clients); err != nil {
		return nil, err
	}
	auth.Signer = manager
	return
}

// TokenClaim is base interface for what is used in GetTokenResponse
type TokenClaim interface {
	SetIssuerIfNeeded(string)
}

// GetTokenResponse returns a token response or an error
func (a *Auth) GetTokenResponse(claims TokenClaim, refreshToken string) (*TokenResponse, error) {
	if claims == nil {
		log.Debug().Msg("Did not authenticate")
		return nil, InvalidRequest
	}
	claims.SetIssuerIfNeeded(a.Issuer)

	// Filtering claims based on client scope
	accessToken, err := MakeJWT(claims, a.Signer)
	if err != nil {
		log.Debug().Err(err).Msg("Could not make JWT")
		return nil, InvalidScope
	}

	response := new(TokenResponse)
	response.AccessToken = accessToken
	response.ExpiresIn = a.Signer.TokenLifetime()
	response.TokenType = "Bearer"
	response.RefreshToken = refreshToken
	return response, nil
}

// FindClient returns a client using a BasicAuth string
// the string must start with Basic
func (a *Auth) FindClient(basicAuth string) (Client, error) {
	username, password, err := utils.ParseBasicAuth(basicAuth)
	if err != nil {
		return nil, err
	}

	client, found := a.Clients[username]
	if !found {
		return nil, fmt.Errorf("no client for id `%s`", username)
	}
	if !client.Verify(password) {
		return nil, fmt.Errorf("invalid password `%s` for id `%s`", password, username)
	}
	return client, nil
}

// SetClients validates the unicity of IDs and Slugs and calls the `Validate` function for each client
func (a *Auth) SetClients(clients []Client) error {
	ids := make(map[string]bool)
	slugs := make(map[string]bool)

	mapped := make(map[string]Client)

	for i := range clients {
		c := clients[i]

		if err := c.Validate(); err != nil {
			return err
		}

		if ids[c.GetID()] {
			return fmt.Errorf("duplicate client id %s", c.GetID())
		}
		ids[c.GetID()] = true

		if slugs[c.GetSlug()] {
			return fmt.Errorf("duplicate client slug %s", c.GetSlug())
		}
		slugs[c.GetSlug()] = true

		mapped[c.GetID()] = c
	}

	a.Clients = mapped
	return nil
}
